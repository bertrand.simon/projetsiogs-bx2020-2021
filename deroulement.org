#+REVEAL_TRANS: None
#+REVEAL_THEME: white
#+REVEAL_EXTRA_CSS: /home/bs/Documents/Enseignement/MixedReality/local.css
#+TITLE: Suivi des projets IOGS-Bx (2020-2021)
#+AUTHOR: Bertrand SIMON
#+EMAIL: bertrand.simon@institutoptique.fr
#+OPTIONS: toc:0 LateX:t
#+EXPORT_FILE_NAME: Projets IOGS Bordeaux 2020-2021
#+OPTIONS: toc:nil H:1 num:nil
#+STARTUP: latexpreview
#+LATEX_class:scrartcl
* Sujets
** Sujets de projets 2A (2020-2021)
*** Hacking d'un spectromètre
 + contact: [[mailto:iste.trabbia@institutoptique.fr][jean-baptiste.trabbia@institutoptique.fr]]

 Le LP2N dispose d’un spectromètre de grande qualité, malheureusement hors-service à cause d’une micro-chute lors d’un transport. Le laboratoire a souhaiée remettre en marche le spectromètre afin de pouvoir le mettre à disposition de ses étudiants lors des travaux pratiques.
 Afin d’y parvenir, le laboratoire a proposé ce projet à un groupe de 5 étudiants de 2ème année qui, au fil des 5 semaines de travail ont tenté de répondre au problème posé. Au début du projet, le spectromètre est complet et ses actionneurs sont fonctionnels. Cependant, le logiciel et la carte de puissance le pilotant sont obsolètes, le spectromètre n’est plus calibré et le module caméra permettant l’acquisition des données est cassé.  A la fin du premier projet, les actionneurs sont fonctionnels et pilotables et la nouvelle carte de puissance fonctionne. Il reste encore aujourd'hui à 
 - choisir et intégrer une caméra
 - améliorer le code de pilotage
 - calibrer l'ensemble
*** Smart 3D
 + contact: [[mailto:bertrand.simon@institutoptique.fr][bertrand.simon@institutoptique.fr]]

 Nous souhaitons pouvoir mesurer la pollution aux métaux lourds présents dans l'eau grâce à un dispositif de micro-fluidique capable de générer un signal de fluorescence dépendant des concentrations de ces métaux. En couplant ce dispositif avec un smartphone, nous espérons caractériser les concentrations de métaux lourds présentes. Le but de ce projet est de concevoir le dispositif de couplage avec le SP (excitation, focalisation, filtrage, détection...) et de caractériser la réponse de l'ensemble en fonction de concentrations de rhodamine équivalentes à celle qui seront observées dans le dispositif final. 
*** FIR: Eye-Tracking / caractérisation du regard / Saccades
 + contact: [[mailto:bertrand.simon@institutoptique.fr][bertrand.simon@institutoptique.fr]] 

 Aujourd'hui les applications AR/VR ont pour enjeu de "coller au plus près" du regard de l'utilisateur. par conséquent les mouvements du regard (saccades), de la tête et leur caractérisation constituent une voie d'amélioration  à travers une meilleure prise en compte des facteurs humains lors de la conception de tels systèmes. Le projet à donc pour but d'explorer ces aspects à travers:
 - un état de l'art et une remise en question des moyens d'eye-tracking
 - une étude des mouvements tête/oeil et leur caractérisation
 - la mise en évidence de "tâches visuelles"
*** Emuler des effets d’optique non-linéaire par une approche opto-numérique (2A ou 3A)
 + contact: [[mailto:nicolas.dubreuil@institutoptique.fr][nicolas.dubreuil@institutoptique.fr]]

 Le régime non-linéaire en optique intervient lors de la propagation d’un faisceau de forte intensité dans un milieu et il est à l’origine d’une multitude d’effets. Dans ce projet, on se propose d’émuler certains effets non-linéaires à l’aide d’une caméra et d'un modulateur spatial de lumière (SLM). La caméra enregistre la distribution d’intensité du faisceau incident, qui est alors traitée numériquement pour commander la variation en phase et/ou l’amplitude qui sera induite par le SLM sur ce même faisceau. En programmant la relation « non-linéaire" entre les variations de phase/amplitude induites par le SLM et la distribution d’intensité, il est possible de reproduire et d’illustrer certains effets non-linéaires tout en travaillant à faible puissance.
 L’objectif serait de développer un banc pour une visée pédagogique et d’étudier la potentialité de cette approche pour illustrer, avec un même banc, une variété d’effets non-linéaires.
*** Réaliser un spectromètre compact à base d’une fibre multimode (2A)
 + contact: [[mailto:nicolas.dubreuil@institutoptique.fr][nicolas.dubreuil@institutoptique.fr]]

 Les techniques classiques pour mesurer le spectre d’une source consistent à utiliser un spectromètre à réseaux (ou un monochromateur) ou des approches interférométriques (interféromètre à transformée de Fourrier, Fabry-Perot confocal…). Récemment, des chercheurs ont réalisé un spectromètre en injectant la source lumineuse dans une fibre optique multimode et en analysant la distribution d’intensité en sortie. Cette dernière résulte des interférences multiples entre tous les modes de la fibre (i.e. tous les « chemins » empruntés par la lumière pour parcourir la fibre). Cette figure d’interférence, qui forme une figure de tavelure (ou speckle en anglais), est très sensible aux caractéristiques spectrales de la source. A partir d’une phase d’apprentissage avec des sources dont le spectre est connu, il est possible par simple traitement d’images de reconstruire le spectre d’une source inconnue. 
 L’intérêt est ici de disposer d’un appareil très compact et très simple (un bout de fibre et une caméra connectée).
*** Logiciel / vidéo effets nano-photonique
 + contact [[mailto:philippe.lalanne@institutoptique.fr][philippe.lalanne@institutoptique.fr]]

 Il s'agirait de faire une code matlab qui visualise le champ dans un nanoresonateur (un bout de metal de 200nm de long) qui pourrait etre proposé ensuite à la fete de la science ou a des portes ouvertes.
 Eventuellement de faire un film.
 - comprendre ce que c'est qu'un mode de resonateur electromagnetique (on fournit les modes)
 - ecrire le petit programme matlab qui dit combien ce mode est excité par un pulse incident (il y a une formule à entrer + une fft à ecrire)
 - faire un petit logiciel qui permet de faire varier le resonateur (microcavité photonique, nanoantenne plasmonique), la durée du pulse, sa frequence centrale, son incidence
 - afficher le champ proche en un point de l'espace ou dans un plan du resonateur) en fonction du temps (cela demande 1s de calcul)
 - comprendre un peu la physique, pour proposer didactiquement à d'autres étudiants d'essager l'outil sur des exemples amusants
 #+DOWNLOADED: screenshot @ 2020-10-06 15:02:22
 #+attr_html: :width 50% :align center
 #+attr_latex: :width 0.75\textwidt
 #+attr_org: :width 256p
 [[file:pictures/Logiciel_/_vidéo_effets_nano-photonique/2020-10-06_15-02-22_screenshot.png]]

*** From telecom lasers to cold atom physics
 + contact: [[mailto:simon.bernon@institutoptique.fr][simon.bernon@institutoptique.fr]]
**** Objectives:
 In this project the group of students is expected to use and characterize a laser system capable of cooling a gaz of atoms to a few hundred microkelvin. Their work will not start from scratch but will build on the achievements of the past years. The generation of the laser radiation is based on a modern technical architecture involving the frequency doubling of a telecom diode laser. To ensure stability, the cooling light generated will be servo locked to the natural transition of Rubidium atoms by a saturated absorption technique. An innovative optical filter, that is key for this laser architecture will be built and characterized by the group of students. Once generated and fully characterized, the laser light will be used to cool a gaz of atoms to approx. 100 $\mu{}K$ and to measure it ! 
 No prerequisite for this project are expected. Laser cooling techniques will be explained by the tutor.
 The student should have strong interest in experimental physics. This project will introduce to the laser cooling and manipulation of cold atoms but also to fiber techniques, laser amplification, non linear process such as laser doubling. The group of student should not be more than 4 and 2 at least.
*** Source multispectrale
 + contact: [[mailto:laurent.cognet@institutoptique.fr][laurent.cognet@institutoptique.fr]]

 Suite à une crise sanitaire, un groupe de projet a dû abandonner la conception d'une source multispectrale originale pour la microscopie. Aujourd'hui la mise en place de cette source revêt une grande importance pour le laboratoire. Votre mission si vous l'acceptez consiste à reprendre le projet pour le mener à bien.
 #+DOWNLOADED: screenshot @ 2020-10-06 15:10:32
 #+attr_html: :width 80% :align center
 #+attr_latex: :width 0.75\textwidt
 #+attr_org: :width 256p
 [[file:pictures/Source_multispectrale/2020-10-06_15-10-32_screenshot.png]]

** Sujets de projets 3A (2020-2021)
*** Réalisation d'un banc d'injection pour des structures nanophtotoniques
+ contact: [[mailto:nicolas.dubreuil@institutoptique.fr][nicolas.dubreuil@institutoptique.fr]]
Le projet consiste à réaliser un banc optique pour caractériser des structures intégrées, comme des guides ou des micro-cavités sur puce. Vous disposerez des éléments nécessaires qu’il faudra monter, aligner puis régler. Le montage fonctionne dans le domaine des longueurs d’onde télécom (1550 nm). L’objectif est de pouvoir caractériser en transmission de telles structures dont les tailles transverses sont inférieures au micromètre.
*** /Deep Learning/ pour la détection de molécules couplées (3A) 
 + contact: [[mailto: jean-baptiste.trabbia@institutoptique.fr][ jean-baptiste.trabbia@institutoptique.fr]]
 Au cours des dernières décennies, l'interaction lumière-matière a fait l'objet de nombreuses études et des efforts théoriques et expérimentaux colossaux ont été déployés pour concevoir et produire des nano-systèmes en forte interaction dans le but de créer des systèmes intriqués où l'excitation est délocalisée sur plusieurs émetteurs. L’une des difficultés principales est la recherche d’émetteurs couplés notamment par la très faible de probabilité de trouver des émetteurs proches et par le manque d’imagerie optique sub-longueur d’onde.
 Je propose, dans ce projet, d’utiliser l’efficacité des algorithmes d’apprentissages profonds (Deep Learning) pour réaliser cette tâche. En effet, les méthodes d’intelligence artificielle (A.I.) sont particulièrement adaptées aux cas où un nombre important de données est collecté et où des caractéristiques spécifiques sur le signal peuvent être reconnues. Nous implémenterons différents types de schémas d’A.I. pour complètement automatiser la recherche comme le LSTM (Long Short Term Memory networks) ou les CNN (Neural Network Convolution).
*** FIR: Perception du mouvement
 suite projet 2A: Perception humaine du mouvement portant en particulier sur la perception du métamérisme du mouvement. Les premières expériences réalisées l'an dernier ont montré une disparité dans la perception "groupée" du mouvement.
*** FIR: Imagerie d'atomes froids par feuille de lumière
 suite projet 2A: Dans le cadre des développement autour de la gravimértie par atomes froids, le laboratoire souhaite imager le nuage d'atome froid par un dispositif de microscopie par feuille de lumière. La phase de conception est maintenant quasiment achevée. Il est attendu un premier prototype de montage expérimental.
*** Classification multi-dimensionnelle des céramiques d'Iznik (3A)
 + [[mailto:xavier.granier@institutoptique.fr][xavier.granier@institutoptique.fr]]
 Le but du sujet et de mettre en place un processus de traitement des carreaux de céramiques d'Iznik pour permettre de les classer. Ce classement a vocation par la suite à être expérimenter pour voir si une classification à partir de critères visuels augmentés corroborerait la provenance géographique qui est une question importante dans l'histoire de l'art.
 Le classement se fera sur critère de colorimétrie, de motifs. Un apprentissage est envisagé.
*** Sujets à choisir (un seul des deux sera encadré)
 [[mailto:bertrand.simon@institutoptique.fr][bertrand.simon@institutoptique.fr]]
1. Affichage 3D en parallaxe (opt.)
 La parallaxe est un élément essentiel de la perception de la profondeur. Elle est utilisée, notamment, dans une application d'affichage sur écran avec un dispositif de suivi des mouvements de la tête de l'utilisateur. Aujourd'hui, Plusieurs librairies permettent de réaliser un suivi de l'orientation de la tête de l'utilisateur. Le but du projet est choisir et d'utiliser l'une de ces librairies pour mettre en place un système d'affichage 3D basé sur la parallaxe.
2. Flou d'accomodation comme indice de profondeur (opt.)
Le flou d'accomodation est un indice pictural de la profondeur. L'absence d'adaptation à l'accomodation dans les dispositifs AR/RV est clairement identifié comme l'une des sources du conflit vergence/accomodation. Il a été montré que les aberrations chromatiques de l'oeil sont utilisées par le système visuel comme indice pour l'accomodation [[http://bankslab.berkeley.edu/publications/Files/chromablur_rendering_chromatic17.pdf][cite:cholewiak17_chrom]]. Le but de ce projet est de mettre en oeuvre un système de rendu permettant de restituer le flou d'accomodation à partir du suivi du regard de l'utilisateur et d'évaluer son impact dans la résolution du conflit vergence/accomodation.
* Répartition
** Répartition 2A
*** Hacking d'un spectromètre (non traité)
*** Smart 3D (complet)
|---------+-----------+------------|
| Prénom  | Nom       | Distanciel |
|---------+-----------+------------|
| Antone  | Kavedjian | *          |
| Elliot  | Cornet    | (*)        |
| Mohamed | Belkadi   |            |
| Alexis  | Eymery    |            |
| Quentin | Gobert    |            |
|---------+-----------+------------|
*** FIR: Eye tracking (non traité)
*** Emuler des effets d’optique non-linéaire par une approche opto-numérique
|---------+--------------------+--------------|
| Prénom  | Nom                | Distanciel ? |
|---------+--------------------+--------------|
| Romain  | Goudet             |              |
| Sebasty | Kevin              |      *       |
| Yanis   | Abdelmoumni-Prunes |      *       |
|---------+--------------------+--------------|
*** Réaliser un spectromètre compact à base d’une fibre multimode (à compléter)
|--------+-------------------+------------|
| Prénom | Nom               | Distanciel ? |
|--------+-------------------+------------|
| Issam  | Belgacem          |       *    |
| Robin  | Mermillod-Blondin |            |
| Yichen | LI                |            |
|--------+-------------------+------------|

*** Logiciel / vidéo effets nano-photonique (non traité)
*** From telecom lasers to cold atom physics
|----------+-------------|
| Nom      | Prénom      |
|----------+-------------|
| Tarek    | Chaieb      |
| Mortada  | Masbah      |
| Laetitia | Martel      |
| Gonzague | de Tanoüarn |
| Clément  | Métayer     |
|----------+-------------|
*** PIM's Optique quantique (Palaiseau)
|-------+---------|
| Nom   | Prénom  |
|-------+---------|
| Julia | Granier |
|-------+---------|
**  Répartition 3A
*** Imagerie de nuages d'AF par feuille de lumière
|------------------------+---------|
| Nom                    | Prénom  |
|------------------------+---------|
| Debavelaere            | Clément |
| Dubois                 | Camille |
| Denoual                | Émilien |
| Santatna Di Figueiredo | Geovan  |
|------------------------+---------|
*** FIR Perception du Mouvement
|----------+--------|
| Nom      | Prénom |
|----------+--------|
| Lefevre  | Vincent [fn:1] |
| Mattatia | Eve    |
| Rognon   | Louis  |
|----------+--------|
*** Classification multi-dimensionnelle des céramiques d’Iznik 
|-----------------+----------|
| Nom             | Prénom   |
|-----------------+----------|
| Lordon          | Blandine |
| Marret          | Alexine  |
| Poillerat       | Nathan   |
| Schwindenhammer | Eliane   |
|-----------------+----------|
*** Deep-Learning pour la détection de molécules couplées (trop de monde !!)
|----------------------+---------|
| Nom                  | Prénom  |
|----------------------+---------|
| Benlarbi             | El Arbi |
| Couturier-Ferreboeuf | Vincent |
| Dubois               | arthur  |
| Grosjean             | Sylvain |
| Khater               | Léa     |
| Poillerat            | Nathan  |
|----------------------+---------|
*** Détection, suivi, et modélisation d'une aile de Kite
|-----------+-----------|
| Nom       | Prénom    |
|-----------+-----------|
| Hale      | Aymeric   |
| Millet    | Charlotte |
| Ollitraut | François  |
|-----------+-----------|
*** Affichage 3D en parallaxe
groupe à confirmer (2éme choix)
|----------+--------|
| Nom      | Prénom |
|----------+--------|
| Labattut | Alexandre[fn:1] |
| Pometti  | Lucas  |
|----------+-----------|
*** Réalisation d’un banc d’injection pour des structures nanophtotoniques (incomplet)
| Nom      | Prénom |
|----------+--------|
| Bourgain | Céline |
| Catrix   | Elias  |
| Bugea    | Thomas |
| Dees     | Stella |
|----------+--------|
*** Flou d'accomodation (non traité) ou à fusionner avec sujet Affichage 3D en parallaxe
*** Footnotes
[fn:1] 2éme choix 
